Eigen STLport no-IO test
========================

The purpose of this test is to see that it is possible to compile a
simple Eigen program using the STLport standard library without IO
streams. This is interesting in embedded applications where no IO is
needed, for example.

Build using

    docker build --tag=stlport .

and run using

    docker run --volume=$HOME/software/eigen/Eigen:/usr/include/Eigen stlport

where $HOME/software/eigen/Eigen should be adjusted to the path to Eigen on your system.
If you don't already have it,

    mkdir -p $HOME/software && \
    cd $HOME/software && \
    hg clone http://bitbucket.org/eigen/eigen
