FROM ubuntu:14.04

ARG STLport_version=STLport-5.2.1

RUN apt-get update && \
    apt-get install --assume-yes wget build-essential cmake

RUN wget --directory-prefix=/tmp \
        "https://downloads.sourceforge.net/project/stlport/STLport/$STLport_version/$STLport_version.tar.bz2" && \
    tar xf "/tmp/$STLport_version.tar.bz2" -C /tmp && \
    ( \
    cd "/tmp/$STLport_version" && \
    ./configure && \
    make && make install && ldconfig \
    ) && \
    rm -r /tmp/*

ADD . /usr/src/eigen-stlport-noio-test

CMD (mkdir /tmp/build && cd /tmp/build && \
    cmake /usr/src/eigen-stlport-noio-test && \
    cmake --build . && \
    ./a.out && \
    echo SUCCESS)
